package com.keta.generate.core;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import com.fly.code.CodeMaker;
import com.keta.generate.util.FreeMarkerUtil;
import com.keta.generate.util.Resources;
import com.keta.generate.util.StringUtil;
import com.keta.generate.vo.Column;
import com.keta.generate.vo.Table;

import freemarker.template.Template;

public class GenerateCode extends AbstractGenerate implements Generate
{
    public GenerateCode()
    {
        super();
    }
    
    /**
     * 生成代码
     * 
     * @param table
     * @throws Exception
     */
    @Override
    public void generate(Table table)
        throws Exception
    {
        generateJava(table);
    }
    
    /**
     * 生成java代码
     * 
     * @param table
     * @throws Exception
     * @see [类、类#方法、类#成员]
     */
    private void generateJava(Table table)
        throws Exception
    {
        try
        {
            // 特殊类型处理
            handleSpecial(table.getColumns());
            model.put("tableName", table.getTableName().toLowerCase());
            model.put("columns", table.getColumns());
            model.put("indexName", "indexName");// for SpringJpa jsp
            model.put("pk", table.getPk());
            model.put("date", new Date());
            model.put("requestMapping", StringUtil.camelCase(table.getTableName(), false).toLowerCase());
            model.put("functionName", "表" + table.getTableName() + "数据");
            URL url = CodeMaker.class.getProtectionDomain().getCodeSource().getLocation();
            logger.info("url={}", url.getPath());
            if (url.getPath().endsWith(".jar")) // 可运行jar内文件处理
            {
                JarFile jarFile = new JarFile(url.getFile());
                Enumeration<JarEntry> entrys = jarFile.entries();
                while (entrys.hasMoreElements())
                {
                    JarEntry jar = (JarEntry)entrys.nextElement();
                    String name = jar.getName();
                    if (!jar.isDirectory() && name.startsWith("template/" + Resources.TPL_FILE_DIR + "/"))
                    {
                        String path = FreeMarkerUtil.renderString(name, model);
                        String realPath = javaPath + StringUtils.substringAfter(path, "template/" + Resources.TPL_FILE_DIR);
                        if (name.endsWith(".ftl"))// 模板文件
                        {
                            realPath = realPath.substring(0, realPath.length() - 4);
                            String ftl = StringUtils.substringAfter(name, "template/" + Resources.TPL_FILE_DIR + "/");
                            Template template = config.getTemplate(ftl);
                            String content = FreeMarkerUtil.renderTemplate(template, model);
                            content = content.replace("$\\{", "${");
                            FileUtils.writeStringToFile(new File(realPath), content, "UTF-8");
                        }
                        else
                        {
                            InputStream inputStream = CodeMaker.class.getResourceAsStream("/" + name);
                            FileUtils.copyInputStreamToFile(inputStream, new File(realPath));
                        }
                    }
                }
                jarFile.close();
            }
            else
            {
                File tFile = new File(url.getFile() + "/template/" + Resources.TPL_FILE_DIR);
                Collection<File> listFiles = FileUtils.listFiles(tFile, null, true);
                for (File file : listFiles)
                {
                    String path = FreeMarkerUtil.renderString(file.getAbsolutePath(), model);
                    String realPath = javaPath + StringUtils.substringAfter(path, "\\template\\" + Resources.TPL_FILE_DIR + "\\");
                    if (realPath.endsWith(".ftl"))
                    {
                        realPath = realPath.substring(0, realPath.length() - 4);
                        String ftl = StringUtils.substringAfter(file.getAbsolutePath(), "\\template\\" + Resources.TPL_FILE_DIR + "\\");
                        Template template = config.getTemplate(ftl);
                        String content = FreeMarkerUtil.renderTemplate(template, model);
                        content = content.replace("$\\{", "${");
                        FileUtils.writeStringToFile(new File(realPath), content, "UTF-8");
                    }
                    else
                    {
                        FileUtils.copyFile(file, new File(realPath));
                    }
                }
            }
        }
        catch (Exception e)
        {
            logger.error(e.getMessage());
            throw e;
        }
    }
    
    @Override
    public void generate(List<Table> tables)
        throws Exception
    {
    }
    
    /**
     * 特殊类型处理
     * 
     * @param columns
     */
    private void handleSpecial(List<Column> columns)
    {
        boolean hasDate = false;
        boolean hasBigDecimal = false;
        for (Column column : columns)
        {
            if ("Date".equals(column.getJavaType()))
            {
                hasDate = true;
            }
            else if ("BigDecimal".equals(column.getJavaType()))
            {
                hasBigDecimal = true;
            }
        }
        model.put("hasDate", hasDate);
        model.put("hasBigDecimal", hasBigDecimal);
    }
}
