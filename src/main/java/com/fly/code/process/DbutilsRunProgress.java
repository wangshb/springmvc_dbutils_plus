package com.fly.code.process;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.keta.generate.core.GenerateFactory;
import com.keta.generate.util.Resources;
import com.keta.generate.util.StringUtil;

/**
 * 
 * 创建代码进度条线程
 * 
 * @author 00fly
 * @version [版本号, 2017年5月3日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class DbutilsRunProgress implements IRunnableWithProgress
{
    private static final Logger LOGGER = LoggerFactory.getLogger(DbutilsRunProgress.class);
    
    private String driver;
    
    private String dburl;
    
    private String username;
    
    private String password;
    
    private String packName;
    
    private String projectDir;
    
    private String[] tabName;
    
    private String prefix;
    
    private String codeType;
    
    public DbutilsRunProgress(String driver, String dburl, String username, String password, String packName, String projectDir, String[] tabName, String prefix, String codeType)
    {
        super();
        this.driver = driver;
        this.dburl = dburl;
        this.username = username;
        this.password = password;
        this.packName = packName;
        this.projectDir = projectDir;
        this.tabName = tabName;
        this.prefix = prefix;
        this.codeType = codeType;
    }
    
    @Override
    public void run(IProgressMonitor monitor)
        throws InvocationTargetException, InterruptedException
    {
        // 在当前目录，创建并运行脚本
        try
        {
            monitor.beginTask("生成代码", IProgressMonitor.UNKNOWN);
            monitor.subTask("自动生成model、dao、service 代码中......");
            creatAndRun(driver, dburl, username, password, packName, projectDir, prefix, tabName);
            monitor.done();
        }
        catch (Exception e)
        {
            LOGGER.error(e.getMessage(), e);
            throw new InvocationTargetException(e.getCause(), e.getMessage());
        }
    }
    
    // 运行代码创建程序
    private void creatAndRun(String driver, String dburl, String username, String password, String packName, String projectDir, String prefix, String[] tabNames)
        throws Exception
    {
        for (String tableName : tabNames)
        {
            String className;
            if (StringUtils.isEmpty(prefix))
            {
                className = StringUtil.camelCase(tableName, true);
            }
            else
            {
                className = StringUtil.camelCase(tableName.replaceAll("(?i)^" + prefix, ""), true);
            }
            Resources.init(driver, dburl, username, password, tableName, packName, projectDir, className, codeType);
            GenerateFactory factory = new GenerateFactory();
            factory.genJavaTemplate();
        }
    }
}