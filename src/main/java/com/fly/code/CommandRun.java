package com.fly.code;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Scanner;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.keta.generate.core.GenerateFactory;
import com.keta.generate.db.Mysql;
import com.keta.generate.util.Resources;
import com.keta.generate.util.StringUtil;
import com.keta.generate.vo.Table;

/**
 * 
 * 命令行交互方式代码生成器
 * 
 * @author 00fly
 * @version [版本号, 2016-12-10]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class CommandRun
{
    private static final Logger LOGGER = LoggerFactory.getLogger(CommandRun.class);
    
    /**
     * 测试
     * 
     * @param args
     * @throws Exception
     * @throws IOException
     * @throws SQLException
     */
    public static void main(String[] args)
        throws Exception
    {
        String input;
        Scanner sc = new Scanner(System.in);
        do
        {
            generateCode("java");
            LOGGER.info("------------输入q退出,输入其他值继续生成代码------------");
            input = StringUtils.trimToEmpty(sc.nextLine());
        } while (!input.equalsIgnoreCase("q"));
        LOGGER.info("----------系统退出成功----------");
        sc.close();
    }
    
    /**
     * generateCode
     * 
     * @param tplFileDir 模板文件目录名
     * @throws Exception
     * @see [类、类#方法、类#成员]
     */
    public static void generateCode(String tplFileDir)
        throws Exception
    {
        String tableName = null;
        String packName = null;
        String className = null;
        ResourceBundle config = ResourceBundle.getBundle("jdbc");
        String driver = config.getString("jdbc.driver");
        String dburl = config.getString("jdbc.url");
        String username = config.getString("jdbc.username");
        String password = config.getString("jdbc.password");
        // JDBC
        Resources.JDBC_DRIVER = driver;
        Resources.JDBC_URL = dburl;
        Resources.JDBC_USERNAME = username;
        Resources.JDBC_PASSWORD = password;
        // 取得数据库表
        List<Table> tables = new Mysql().getTables();
        Scanner sc = new Scanner(System.in);
        boolean success = false;
        List<String> tableToSelect = new ArrayList<>();
        do
        {
            if (tableToSelect.isEmpty())
            {
                for (Table table : tables)
                {
                    tableToSelect.add(table.getTableName());
                }
            }
            // 显示表名供选择
            int index = 1;
            for (String it : tableToSelect)
            {
                LOGGER.info("{}:{}", index, it);
                index++;
            }
            LOGGER.info("请输入数据库表名过滤后按序号选择数据库表");
            String input = StringUtils.trimToEmpty(sc.nextLine());
            if (StringUtils.isNumeric(input))
            {
                int selectIndex = NumberUtils.toInt(input);
                if (selectIndex > 0 && selectIndex <= tableToSelect.size())
                {
                    tableName = tableToSelect.get(selectIndex - 1);
                    success = true;
                }
            }
            tableToSelect.clear();
            if (!success)
            {
                for (Table table : tables)
                {
                    if (table.getTableName().equals(input))
                    {
                        tableName = input;
                        success = true;
                    }
                    else if (table.getTableName().contains(input))
                    {
                        tableToSelect.add(table.getTableName());
                    }
                }
            }
        } while (!success);
        LOGGER.info("你选择了数据库表：{} \n", tableName);
        // 取java包路径
        do
        {
            LOGGER.info("请输入java包路径,回车使用默认值com.fly.demo：");
            packName = StringUtils.defaultIfEmpty(sc.nextLine(), "com.fly.demo");
        } while (StringUtils.isEmpty(packName));
        LOGGER.info("你选择了java包路径：{} \n", packName);
        // 取类名称
        do
        {
            String defaultclass = StringUtil.camelCase(tableName, true);
            LOGGER.info("请输入类名,回车使用默认值{}：", defaultclass);
            className = StringUtils.defaultIfEmpty(sc.nextLine(), defaultclass);
            className = StringUtil.camelCase(className, true);
        } while (StringUtils.isEmpty(className));
        LOGGER.info("你选择了类名：{}\n", className);
        LOGGER.info("请确认以下信息：");
        LOGGER.info("1.数据库表{}", tableName);
        LOGGER.info("2.包路径：{}", packName);
        LOGGER.info("3.类名：{}", className);
        LOGGER.info("回车继续,其他取消");
        if (StringUtils.isEmpty(sc.nextLine()))
        {
            // 代码生成
            String srcDir = new File("code").getAbsolutePath();
            try
            {
                // 清空目录下的文件
                File srcFile = new File(srcDir);
                if (srcFile.exists())
                {
                    FileUtils.cleanDirectory(srcFile);
                }
            }
            catch (IOException e)
            {
                LOGGER.error(e.getMessage(), e);
            }
            LOGGER.info("------------开始生成代码------------");
            Resources.init(driver, dburl, username, password, tableName, packName, srcDir, className, tplFileDir);
            GenerateFactory factory = new GenerateFactory();
            factory.genJavaTemplate();
            LOGGER.info("------------生成代码结束------------");
        }
        else
        {
            LOGGER.info("------------你成功取消代码生成------------");
        }
        sc.close();
    }
}