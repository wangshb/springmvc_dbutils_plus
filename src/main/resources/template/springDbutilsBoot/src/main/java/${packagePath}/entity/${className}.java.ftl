package ${pknEntity};

<#if hasDate == true>
import java.util.Date;
</#if>
<#if hasBigDecimal == true>
import java.math.BigDecimal;
</#if>
import org.hibernate.validator.constraints.NotBlank;

/**
 * 
 * ${tableName}表对应的${className}实体
 * 
 * @author 00fly
 * @version [版本号, ${date?date}]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class ${className}
{
    // ${pk.name}${pk.comment}
    private ${pk.javaType} ${pk.fieldName};
    
    <#list columns as column>
    // ${column.name}${column.comment}<#if column.javaType='String'><#if !column.nullable>
    @NotBlank(message = "${column.name}不能为空")</#if></#if>
    private ${column.javaType} ${column.fieldName};
    
    </#list>
    public ${pk.javaType} ${pk.getMethod}()
    {
        return ${pk.fieldName};
    }
    
    public void ${pk.setMethod}(${pk.javaType} ${pk.fieldName})
    {
        this.${pk.fieldName} = ${pk.fieldName};
    }
	<#list columns as column>
    
    public void ${column.setMethod}(${column.javaType} ${column.fieldName})
    {
        this.${column.fieldName} = ${column.fieldName};
    }
    
    public ${column.javaType} ${column.getMethod}()
    {
        return this.${column.fieldName};
    }
	</#list>
}
