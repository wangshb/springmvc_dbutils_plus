package	${pknServiceImpl};

import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ${packageName}.common.PaginationSupport;
import ${packageName}.core.TransactionProvider;
import ${pknDAO}.${className}DAO;
import ${pknDAO}.impl.${className}DAOImpl;
import ${pknEntity}.${className};
import ${pknService}.${className}Service;

/**
 * 
 * ${className}Service 接口实现类
 * 
 * @author 00fly
 * @version [版本号, ${date?date}]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class ${className}ServiceImpl extends TransactionProvider implements ${className}Service 
{
    
    private static final Logger LOGGER = LoggerFactory.getLogger(${className}ServiceImpl.class);
    
	private ${className}DAO ${instanceName}DAO = ${className}DAOImpl.getInstance();
    
    private ${className}ServiceImpl()
    {
        super();
    }
    
    private static final ${className}ServiceImpl SINGLE = new ${className}ServiceImpl();
    
    /**
     * 单例获取对象
     * 
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static ${className}ServiceImpl getInstance()
    {
        return SINGLE;
    }
    
    @Override
    public void insert(${className} ${instanceName})
    {
        ${instanceName}DAO.insert(${instanceName});
    }
    
    @Override
    public void deleteById(${pk.javaType} id)
    {
        ${instanceName}DAO.deleteById(id);
    }
    
    @Override
    public long deleteById(${pk.javaType}[] ids)
    {
        return ${instanceName}DAO.deleteById(ids);
    }
    
    @Override
    public long deleteById(List<${pk.javaType}> ids)
    {
        return ${instanceName}DAO.deleteById(ids);
    }
    
    @Override
    public void update(${className} ${instanceName})
    {
        ${instanceName}DAO.updateById(${instanceName});
    }
    
    @Override
    public void updateSelective(${className} ${instanceName})
    {
        ${instanceName}DAO.updateByIdSelective(${instanceName});
    }
    
    @Override
    public void saveOrUpdate(${className} ${instanceName})
    {
        <#if pk.javaType!='String'>if (${instanceName}.${pk.getMethod}() == null)
        {
            ${instanceName}DAO.insert(${instanceName});
        }</#if><#if pk.javaType='String'>if (StringUtils.isBlank(${instanceName}.${pk.getMethod}()))
        {
        	${instanceName}.${pk.setMethod}(UUID.randomUUID().toString());
            ${instanceName}DAO.insert(${instanceName});
        }</#if>
        else
        {
            ${instanceName}DAO.updateById(${instanceName});
        }
    }
    
    @Override
    public ${className} queryById(${pk.javaType} id)
    {
        return ${instanceName}DAO.queryById(id);
    }
    
    @Override
    public List<${className}> queryAll()
    {
        return ${instanceName}DAO.queryAll(); 
    }
    
    
    /**
     * 根据条件分页查询
     * 
     * @param ${instanceName} 条件对象
     * @param pageNo 页号
     * @param pageSize 页大小
     * @return
     */
    @Override
    public PaginationSupport<${className}> queryForPagination(${className} ${instanceName}, int pageNo, int pageSize)
    {
        return ${instanceName}DAO.queryForPagination(${instanceName}, pageNo, pageSize);
    }
    
    /**
     * 事务方法
     * 
     * @see [类、类#方法、类#成员]
     */
    public void testTrans()
    {
        try
        {
            startTransaction();
            List<${className}> list = queryAll();
            for (${className} ${instanceName}: list)
            { 
                ${instanceName}DAO.insert(${instanceName});
            }
            Assert.assertTrue(false); // 抛出异常
            commitAndClose();
        }
        catch (Exception e)
        {
            rollbackAndClose();
            LOGGER.error(e.getMessage(), e);
        }
    }
}
