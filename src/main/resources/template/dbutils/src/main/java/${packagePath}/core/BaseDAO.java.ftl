package ${packageName}.core;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.dbutils.GenerousBeanProcessor;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.RowProcessor;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ${packageName}.common.PaginationSupport;

/**
 * 
 * BaseDAO
 * 
 * @author 00fly
 * @version [版本号, ${date?date}]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@SuppressWarnings({"rawtypes", "hiding", "unchecked"})
public class BaseDAO<T>
{
    private static final Logger LOGGER = LoggerFactory.getLogger(BaseDAO.class);
    
    private QueryRunner runner = new QueryRunner();
    
    public BaseDAO()
    {
        super();
    }
    
    /**
     * 批量更新
     * 
     * @param sql 需执行的sql
     * @param params 参数组
     * @return 顺序返回执行后所影响到的行数
     * @throws SQLException
     */
    public int[] batch(String sql, Object[][] params)
        throws SQLException
    {
        LOGGER.info("executeBatch: {}, params:{}", sql, params);
        Connection conn = null;
        try
        {
            conn = MySqlDBUtil.getConnection();
            return runner.batch(conn, sql, params);
        }
        finally
        {
            if (conn != null && conn.getAutoCommit())
            {
                MySqlDBUtil.close(conn);
            }
        }
    }
    
    /**
     * 批量更新
     * 
     * @param sql 需执行的sql
     * @param params List参数组
     * @return 顺序返回执行后所影响到的行数
     * @throws SQLException
     */
    public int[] batch(String sql, List<Object[]> params)
        throws SQLException
    {
        Object[][] paramArr = params.toArray(new Object[0][]);
        return batch(sql, paramArr);
    }
    
    /**
     * 带可变参数, 执行sql插入，返回新增记录的自增主键<BR>
     * 注意： 若插入的表无自增主键则返回 0，异常的话则返回 null
     * 
     * @param sql 需执行的sql
     * @param para 可变参数
     * @return
     * @throws SQLException
     */
    public Long insert(String sql, Object... para)
        throws SQLException
    {
        LOGGER.info("InsertSql: {}, para: {}", sql, para);
        Connection conn = null;
        try
        {
            conn = MySqlDBUtil.getConnection();
            return (Long)runner.insert(conn, sql, new ScalarHandler<Object>(), para);
        }
        finally
        {
            if (conn != null && conn.getAutoCommit())
            {
                MySqlDBUtil.close(conn);
            }
        }
    }
    
    /**
     * 带可变参数查询,返回执行结果
     * 
     * @param sql 查询sql
     * @param para 可变参数
     * @return
     * @throws SQLException
     */
    public List<Map<String, Object>> query(String sql, Object... para)
        throws SQLException
    {
        LOGGER.info("querySql: {}, para: {}", sql, para);
        Connection conn = null;
        try
        {
            conn = MySqlDBUtil.getConnection();
            return runner.query(conn, sql, new MapListHandler(), para);
        }
        finally
        {
            if (conn != null && conn.getAutoCommit())
            {
                MySqlDBUtil.close(conn);
            }
        }
    }
    
    /**
     * 带可变参数查询,返回执行结果
     * 
     * @param clazz
     * @param sql 查询sql
     * @param para 可变参数
     * @return
     * @throws SQLException
     */
    public <T> List query(T clazz, String sql, Object... para)
        throws SQLException
    {
        LOGGER.info("querySql: {}, para: {}", sql, para);
        Connection conn = null;
        try
        {
            conn = MySqlDBUtil.getConnection();
            // 下划线分隔的表字段名转换为实体bean驼峰命名属性
            BeanProcessor bean = new GenerousBeanProcessor();
            RowProcessor processor = new BasicRowProcessor(bean);
            return (List)runner.query(conn, sql, new BeanListHandler((Class)clazz, processor), para);
        }
        finally
        {
            if (conn != null && conn.getAutoCommit())
            {
                MySqlDBUtil.close(conn);
            }
        }
    }
    
    /**
     * 带可变参数查询,返回首条执行结果
     * 
     * @param sql 查询sql
     * @param para 可变参数
     * @return
     * @throws SQLException
     */
    public Map<String, Object> queryFirst(String sql, Object... para)
        throws SQLException
    {
        LOGGER.info("querySql: {}, para: {}", sql, para);
        Connection conn = null;
        try
        {
            conn = MySqlDBUtil.getConnection();
            return runner.query(conn, sql, new MapHandler(), para);
        }
        finally
        {
            if (conn != null && conn.getAutoCommit())
            {
                MySqlDBUtil.close(conn);
            }
        }
    }
    
    /**
     * 带可变参数查询,返回首条执行结果
     * 
     * @param clazz
     * @param sql 查询sql
     * @param para 可变参数
     * @return
     * @throws SQLException
     */
    public <T> Object queryFirst(T clazz, String sql, Object... para)
        throws SQLException
    {
        if (!sql.toLowerCase().contains(" limit ")) // 前后有空格
        {
            sql = sql + " limit 1";
        }
        List<T> list = query((Class)clazz, sql, para);
        if (list.isEmpty())
        {
            return null;
        }
        return list.get(0);
    }
    
    /**
     * 带可变参数查询，返回long类型数据
     * 
     * @param countSql 查询记录条数的sql
     * @param para 可变参数
     * @return
     * @throws SQLException
     */
    public Long queryForLong(String countSql, Object... para)
        throws SQLException
    {
        LOGGER.info("queryForLong: {}, para: {}", countSql, para);
        Connection conn = null;
        try
        {
            conn = MySqlDBUtil.getConnection();
            return runner.query(conn, countSql, new ScalarHandler<Long>(), para);
        }
        finally
        {
            if (conn != null && conn.getAutoCommit())
            {
                MySqlDBUtil.close(conn);
            }
        }
    }
    
    /**
     * 带可变参数条件的分页查询
     * 
     * @param sql 查询sql
     * @param pageNo 页号
     * @param pageSize 每页记录数
     * @param para 可变参数
     * @return
     * @throws SQLException
     */
    public PaginationSupport queryForPagination(String sql, int pageNo, int pageSize, Object... para)
        throws SQLException
    {
        // 保证正整数
        pageNo = Math.max(pageNo, 1);
        pageSize = Math.max(pageSize, 1);
        // 查询记录总条数
        int index = sql.toLowerCase().indexOf(" from ");
        String countSql = "select count(1)" + StringUtils.substring(sql, index);
        long total = queryForLong(countSql, para);
        // 查询当前页数据
        StringBuilder sbSql = new StringBuilder(sql).append(" limit ").append(pageSize * (pageNo - 1)).append(", ").append(pageSize);
        List<Map<String, Object>> list = query(sbSql.toString(), para);
        // 封装返回分页对象
        PaginationSupport page = new PaginationSupport(total, pageNo, pageSize);
        page.setItems(list);
        return page;
    }
    
    /**
     * 带可变参数条件的分页查询
     * 
     * @param clazz
     * @param sql 查询sql
     * @param pageNo 页号
     * @param pageSize 每页记录数
     * @param para 可变参数
     * @return
     * @throws SQLException
     */
    public <T> PaginationSupport queryForPagination(T clazz, String sql, int pageNo, int pageSize, Object... para)
        throws SQLException
    {
        // 保证正整数
        pageNo = Math.max(pageNo, 1);
        pageSize = Math.max(pageSize, 1);
        // 查询记录总条数
        int index = sql.toLowerCase().indexOf(" from ");
        String countSql = "select count(1)" + StringUtils.substring(sql, index);
        long total = queryForLong(countSql, para);
        // 查询当前页数据
        StringBuilder sbSql = new StringBuilder(sql).append(" limit ").append(pageSize * (pageNo - 1)).append(", ").append(pageSize);
        List<T> list = query((Class)clazz, sbSql.toString(), para);
        // 封装返回分页对象
        PaginationSupport page = new PaginationSupport(total, pageNo, pageSize);
        page.setItems(list);
        return page;
    }
    
    /**
     * 带可变参数, 执行sql，返回执行影响的记录条数
     * 
     * @param sql 执行的sql 语句
     * @param para 可变参数
     * @return
     * @throws SQLException
     */
    public int update(String sql, Object... para)
        throws SQLException
    {
        LOGGER.info("executeUpdate: {}, para: {}", sql, para);
        Connection conn = null;
        try
        {
            conn = MySqlDBUtil.getConnection();
            return runner.update(conn, sql, para);
        }
        finally
        {
            if (conn != null && conn.getAutoCommit())
            {
                MySqlDBUtil.close(conn);
            }
        }
    }
}
