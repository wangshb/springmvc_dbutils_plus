package ${pknDAO};

import java.util.List;

import ${packageName}.common.PaginationSupport;
import ${pknEntity}.${className};

/**
 * 
 * ${className}DAO 接口
 * 
 * @author 00fly
 * @version [版本号, ${date?date}]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public interface ${className}DAO
{   

    /**
     * 根据条件删除数据
     * 
     * @param criteria 条件对象
     * @return
     */
    long deleteByCriteria(${className} criteria);
    
    /**
     * 根据主键id删除数据
     * 
     * @param id 主键
     * @return
     */
    long deleteById(${pk.javaType} id);
    
    /**
     * 根据主键id列表删除数据
     * 
     * @param ids 主键列表
     * @return
     */
    long deleteById(${pk.javaType}[] ids);
    
    /**
     * 根据主键id列表删除数据
     * 
     * @param ids 主键列表
     * @return
     */
    long deleteById(List<${pk.javaType}> ids);
    
    /**
     * 增加记录(插入全字段)
     * 
     * @param bean 待插入对象
     * @return
     */
    boolean insert(${className} bean);
    
    /**
     * 增加记录(仅插入非空字段)
     * 
     * @param bean 待插入对象
     * @return
     */
    boolean insertSelective(${className} bean);
    
    /**
     * 查询全部
     * 
     * @return
     */
    List<${className}> queryAll();
    
    /**
     * 根据条件查询
     * 
     * @param criteria 条件对象
     * @return
     */
    List<${className}> queryByCriteria(${className} criteria);
    
    /**
     * 根据id查找数据
     * 
     * @param id 主键
     * @return
     */
    ${className} queryById(${pk.javaType} id);
    
    /**
     * 根据条件分页查询
     * 
     * @param criteria 条件对象
     * @param pageNo 页号
     * @param pageSize 页大小
     * @return
     */
    PaginationSupport<${className}> queryForPagination(${className} criteria, int pageNo, int pageSize);
    
    /**
     * 根据条件查询数据条数
     * 
     * @param criteria 条件对象
     * @return
     */
    long queryTotal(${className} criteria);
    
    /**
     * 根据复杂条件更新全字段数据
     * 
     * @param bean 待更新对象
     * @param criteria 条件对象
     * @return
     */
    boolean updateByCriteria(${className} bean, ${className} criteria);
    
    /**
     * 根据复杂条件更新非空字段数据
     * 
     * @param bean 待更新对象
     * @param criteria 条件对象
     * @return
     */
    boolean updateByCriteriaSelective(${className} bean, ${className} criteria);
    
    /**
     * 根据id更新全部数据
     * 
     * @param bean 待更新对象
     * @return
     */
    boolean updateById(${className} bean);
    
    /**
     * 根据id更新非空字段数据
     * 
     * @param bean 待更新对象
     * @return
     */
    boolean updateByIdSelective(${className} bean);
}
